/*    SLIDER    */

var index = 0;
var images = [];
var time = 4000;

images[0] = "img/Screenshot from 2020-06-18 14-20-35.png";  /* Array mit Bildern, die durchgeschaltet werden sollen */
images[1] = "img/Screenshot from 2020-06-18 14-25-43.png";
images[2] = "img/Screenshot from 2020-06-18 12-59-05.png";

function changeImg() {  /* Ändere das Bild */
  document.slide.src = images[index];     /* Ändere die Quelle des img in html auf die Verknüpfung des Bildes in dem aktuellen Arraywert */
  if(index < images.length -1){   /* wenn es noch ein Bild gibt in dem Array.. */
    index++;                      /* ..wechsle dorthin... */
  } else {                        /* wenn nicht, ...*/
    index = 0;                    /* beginne wieder von Vorne */
  }
  setTimeout("changeImg()", time);    /* führe die Funktion changeImg() alle time millisekunden aus */ 
}

window.onload = changeImg;     /* führt changeImg aus, nach dem Laden der Seite */



/*    GREETING    */

var now = new Date();
var Hour = now.getHours();    /* Hour = Uhrzeit(Stunde) beim Client */

if (Hour >= 4 && Hour <= 11)                                      /* Für jede Uhrzeit(Stunde) bestimmte Begrüßungen in die html(id="greeting") einfügen */
  ("#greeting").html("Good morning!");
else if (Hour >= 12 && Hour <= 17)
  $("#greeting").html("Good afternoon!");
else if (Hour >= 18 && Hour <=24 ||	Hour <= 4 && Hour >= 0)
  $("#greeting").html("Good evening!");
else
  $("#greeting").html("Welcome");                                 /* Wenn keine Uhrzeit ermittelt werden kann o. Ä. wird eine neutrale Begrüßung eingefügt */



/*    BACK TO TOP BUTTON    */

window.onscroll = function() {scrollFunction()};    // Wenn gescrollt wird, führe scrollFuntion aus für konstante abfrage

function scrollFunction() {
  if ($(document).scrollTop() > 200) {              // zeige den Back-to-Top Button an, wenn der User weiter als 200 Pixel runter gescrollt hat..
    $("#bttbutton").css("display","block");
  } else {
    $("#bttbutton").css("display","none");          // .. wenn nicht, verstecke den Back-to-Top Button
  }
}

function btt() {                                    // scrollt nach oben wenn geklickt
  window.scrollTo(0,0);
}